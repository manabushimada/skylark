# Skylark app

What things showing off
* **API management**: Fetch JSON data, error handling etc
* **Big data handling**: Well-organised JSON data buckets
* **Custom classes**: TableView and Extension customization

## To build

Run the following command:

`$ pod install`

Then, open `Skylark.xcworkspace` and run/build the scheme.

## Systems

*TabBar Items*

* **Home page**: access the API - `/api/sets` to show the contents on TableView
* **Episodes page**: access the API - `/api/episodes` to display the contents on CollectionView

**Attention**: *In order to create like an actual app, I am using dummy images when the api does not hold image url.*

![Ref Image](http://i.imgur.com/yjPlAzE.png?1)

![Ref Image](http://i.imgur.com/uTHWvvA.png?1)

## Author
manabu shimada
- http://manabushimada.com/
- https://github.com/manabushimada/
- https://soundcloud.com/manabushimada
