//
//  NetworkDataManager.swift
//  Skylark
//
//  Created by manabu shimada on 13/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

let timeout: NSTimeInterval = 60

private let _sharedInstance = NetworkManager()

class NetworkManager
{
    
    private init() {
        
    }
    
    class var sharedInstance: NetworkManager {
        return _sharedInstance
    }
    
    
    var manager = Alamofire.Manager.sharedInstance

    func requestWithURL(url: String, completion: (response: Response<AnyObject, NSError>) -> Void)
    {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = timeout // seconds
        configuration.timeoutIntervalForResource = timeout
        self.manager = Alamofire.Manager(configuration: configuration)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.manager.request(.GET, url).responseJSON { response in
                completion(response:response)
            }
        }
    }
    
    
    func querySkylarkHomeSetsAPI(completion: (response: Response<AnyObject, NSError>) -> Void)
    {
        self.requestWithURL(Skylark.setsAPI, completion:{ response in
            completion(response:response)
        })
    }
    
    
    func querySkylarkHomeEpisodesAPI(completion: (response: Response<AnyObject, NSError>) -> Void)
    {
        self.requestWithURL(Skylark.episodesAPI, completion:{ response in
            completion(response:response)
        })
    }
    
    
    
    
    func getObjects(json:JSON, completion: (json: JSON) -> Void)
    {
        dispatch_async(dispatch_get_main_queue()) {
            json[Skylark.sets.objects].forEach { (_, json) in
                completion(json: json)
            }
        }
    }
    


}



