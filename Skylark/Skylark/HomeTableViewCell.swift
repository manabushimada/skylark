//
//  HomeTableViewCell.swift
//  Skylark
//
//  Created by manabu shimada on 14/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell
{
    @IBOutlet weak var setsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var createdByLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        //First Call Super
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        print(self.titleLabel.text)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
    }
    
    func updateTitleLabel(string: String)
    {
        self.titleLabel.text = (string == "null") ? "Untitled" : string
    }
    
    func updateCreatedByLabel(string: String)
    {
        self.createdByLabel.text = (string == "null") ? "Unknown" : string
    }
    
    func updateCreatedAtLabel(string: String)
    {
        // Make it readable for human
        let newString = string.substringByIndex(10)
//        let dateFor: NSDateFormatter = NSDateFormatter()
//        dateFor.dateFormat = "yyyy-MM-dd" //'T'HH:mm:ss.SSSZ
//        let yourDate: NSDate? = dateFor.dateFromString(substring)
        self.createdAtLabel.text = (string == "null") ? "No date" : newString
        

        
        
        
        
    }
    
    
    
    
    
    
    
}
