//
//  EpisodesTableViewCell.swift
//  Skylark
//
//  Created by manabu shimada on 15/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit

class EpisodesCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var episodeImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}


