//
//  EpisodesTableViewController.swift
//  Skylark
//
//  Created by manabu shimada on 13/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EpisodesCollectionViewController: UICollectionViewController
{
    let refreshControl = UIRefreshControl()
    var indicator: UIActivityIndicatorView!
    
    var objects = [JSON]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.collectionView!.alwaysBounceVertical = true
        
        self.setups()
        self.indicator.startAnimating()
        self.querySkylarkHomeEpisodesAPI()
    }
    
    
    
    
    // Skylark API
    func querySkylarkHomeEpisodesAPI()
    {
        NetworkManager.sharedInstance.requestWithURL(Skylark.episodesAPI, completion:{ response in
            
            if response.result.isFailure
            {
                self.showNetworkErrorAlertView(response)
            }
            
            guard let object = response.result.value else {
                return
            }
            
            let json = JSON(object)
            //print(json)
            json[Skylark.sets.objects].forEach { (_, json) in
                self.objects.append(json)
            }
            
            self.indicator.stopAnimating()
            self.collectionView!.reloadData()
        })
        
    }
    
    func getSetsImageURL(imageAPI: String,  completion: (imageURL: String) -> Void)
    {
        NetworkManager.sharedInstance.requestWithURL(Skylark.api+imageAPI, completion:{ response in
            
            if response.result.isFailure
            {
                self.showNetworkErrorAlertView(response)
            }
            
            guard let object = response.result.value else {
                return
            }
            
            let json = JSON(object)
            completion(imageURL: String(json[Skylark.sets.images.url]))
        })
    }
    
    
    
    
    
    
    // MARK: - UICollectionViewDelegate Protocol
    
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // MARK: UICollectionViewDataSource delegate
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.objects.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) ->UICollectionViewCell
    {
        let cell:EpisodesCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! EpisodesCollectionViewCell
        
        // Set image
        let imageAPI = String(self.objects[indexPath.row][Skylark.episodes.imageURLs][0])
        //print(self.objects[indexPath.row][Skylark.episodes.imageURLs])
        if imageAPI == "null"
        {
            // random images if no image
            cell.episodeImageView.image = UIImage(named:Dummy.images[indexPath.row%Dummy.images.count])
        }
        else
        {
            self.getSetsImageURL(imageAPI, completion: { imageURL in
                cell.episodeImageView.sd_setImageWithURL(NSURL(string: imageURL)!)
            })
        }
        
        
        
        return cell

        
    }

    
    // MARK: Private Methods
    func showNetworkErrorAlertView(response: Response<AnyObject, NSError>)
    {
        self.indicator.stopAnimating()
        
        switch response.result
        {
        case .Failure(let encodingError):
            //print(encodingError)
            switch encodingError.code
            {
            case -1001, -1009: // 1009: offline, 1001: timeout
                let alertController = UIAlertController(title: Messages.alert.connectionErrorTitle, message:
                    Messages.alert.connectionErrorMessage, preferredStyle: UIAlertControllerStyle.Alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                    
                }))
                presentViewController(alertController, animated: true, completion: nil)
                break
            case 3840: // invalid values
                /**
                 let alertController = UIAlertController(title: Messages.alert.invalidValues, message:
                 nil, preferredStyle: UIAlertControllerStyle.Alert)
                 
                 alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                 }))
                 presentViewController(alertController, animated: true, completion: nil)
                 */
                break
            default:
                break
                
            }
        default:
            break
        }
    }
    
    func setups()
    {
        /*----------------------------------------------------------------------------*
         * Add an indicator
         *----------------------------------------------------------------------------*/
        self.indicator = UIActivityIndicatorView()
        self.indicator.frame = CGRectMake(0, 0, 50, 50)
        self.indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        self.indicator.center = self.view.center
        self.collectionView!.addSubview(self.indicator)
        
        
        /*----------------------------------------------------------------------------*
         * Add a refreshcontrol
         *----------------------------------------------------------------------------*/
        //self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(EpisodesCollectionViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView!.addSubview(self.refreshControl)
        
    }
    
    func refresh()
    {
        self.querySkylarkHomeEpisodesAPI()
        self.refreshControl.endRefreshing()
    }
    
    
    
    // MARK: Overrides
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        let cell = sender as! EpisodesCollectionViewCell
        let indexPath = self.collectionView!.indexPathForCell(cell)
        if (segue.identifier == "toEpisodesViewController")
        {
            /*----------------------------------------------------------------------------*
             * Pass the selected cell data.
             *----------------------------------------------------------------------------*/
            let vc = segue.destinationViewController as! EpisodesViewController
            vc.object = self.objects[indexPath!.row].dictionaryObject
            vc.object![Skylark.sets.downloadedImage] = cell.episodeImageView.image
        }
    }
    
    
}
