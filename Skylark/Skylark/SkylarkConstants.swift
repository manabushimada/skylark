//
//  SkylarkConstants.swift
//  Skylark
//
//  Created by manabu shimada on 13/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit

/*----------------------------------------------------------------------------*
 * Skylark Constants.
 *----------------------------------------------------------------------------*/
struct Skylark
{
    static let api = "http://feature-code-test.skylark-cms.qa.aws.ostmodern.co.uk:8000"
    
    struct endpoints
    {
        static let sets = "/api/sets/"
        static let episodes = "/api/episodes/"
    }
    
    static let setsAPI = Skylark.api+Skylark.endpoints.sets
    static let episodesAPI = Skylark.api+Skylark.endpoints.episodes
    
    struct sets
    {
        static let objects = "objects"
        static let title = "title"
        static let items = "items"
        static let contentURL = "content_url"
        static let createdBy = "created_by"
        static let created = "created"
        static let imageURLs = "image_urls"
        static let summary = "summary"
        static let uid = "uid"
        static let body = "body"
        
        struct images
        {
            static let url = "url"
        }
        
        static let downloadedImage = "downloaded_image"
    }
    
    struct episodes
    {
        static let title = "title"
        static let imageURLs = "image_urls"
        static let slug = "slug"
        static let versionURL = "version_url"
        static let created = "created"
        static let uid = "uid"
        static let setTypeSlug = "set_type_slug"
        
        static let downloadedImage = "downloaded_image"
    } 
}

struct Messages
{
    struct alert
    {
        static let connectionErrorTitle = "Please check your network conditions"
        static let connectionErrorMessage = "Pull the table to refresh"
        
        static let invalidValues = "The value is no valid"
    }
 
    
}








/** sets
uid: "coll_20ce39424470457c925f823fc150b3d4",
schedule_urls: [
"/api/schedules/sche_aeba759af1f44c9ca75564c363c870b6/"
],
publish_on: "2014-10-24T00:00:00+00:00",
quoter: "",
featured: false,
language_modified_by: null,
plans: [ ],
cached_film_count: 0,
modified_by: null,
temp_id: 91,
title: "Popular",
self: "/api/sets/coll_20ce39424470457c925f823fc150b3d4/",
created_by: "admin@example.com",
language_publish_on: "2014-10-24T00:00:00+00:00",
language_modified: "2014-10-25T12:45:54.159000+00:00",
has_price: false,
set_type_url: "/api/set-types/sett_888fea5fb5474ee9aac9416f4fd6bce6/",
temp_image: "",
film_count: 0,
body: "",
language_version_url: "/api/sets/coll_20ce39424470457c925f823fc150b3d4/language-versions/0/",
quote: "",
lowest_amount: null,
formatted_body: "",
image_urls: [ ],
hierarchy_url: null,
schedule_url: "/api/schedules/sche_aeba759af1f44c9ca75564c363c870b6/",
active: true,
slug: "popular",
version_number: 0,
language_ends_on: "2100-01-01T00:00:00+00:00",
created: "2014-10-25T12:45:54+00:00",
items: [ ],
language_version_number: 0,
modified: "2014-10-25T12:45:54.159000+00:00",
summary: "",
ends_on: "2100-01-01T00:00:00+00:00",
version_url: "/api/sets/coll_20ce39424470457c925f823fc150b3d4/versions/0/",
set_type_slug: "listings"
*/



/** episodes
 subtitle: null,
 uid: "film_5833235884af4ce6b1e7077949a47655",
 schedule_urls: [
 "/api/schedules/sche_aeba759af1f44c9ca75564c363c870b6/"
 ],
 image_urls: [ ],
 publish_on: "2014-10-24T00:00:00+00:00",
 talent_urls: [ ],
 schedule_url: "/api/schedules/sche_aeba759af1f44c9ca75564c363c870b6/",
 plan_urls: [ ],
 language_publish_on: "2014-10-24T00:00:00+00:00",
 episode_number: null,
 language_modified_by: null,
 slug: "sea-dreams-1914",
 language_version_url: "/api/episodes/film_5833235884af4ce6b1e7077949a47655/language-versions/0/",
 version_number: 0,
 modified_by: null,
 language_ends_on: "2100-01-01T00:00:00+00:00",
 title: "Sea Dreams",
 items: [
 "/api/assets/asse_7d92084d120f45bfa325a60176e8c513/"
 ],
 self: "/api/episodes/film_5833235884af4ce6b1e7077949a47655/",
 created: "2014-10-25T14:35:27.670000+00:00",
 modified: "2014-10-25T14:35:27.670000+00:00",
 created_by: null,
 tag_urls: [ ],
 ends_on: "2100-01-01T00:00:00+00:00",
 synopsis: "",
 version_url: "/api/episodes/film_5833235884af4ce6b1e7077949a47655/versions/0/",
 parent_url: null,
 language_version_number: 0,
 language_modified: "2014-10-25T14:35:27.670000+00:00"
 */