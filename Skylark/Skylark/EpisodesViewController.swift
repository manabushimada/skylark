//
//  EpisodesViewController.swift
//  Skylark
//
//  Created by manabu shimada on 15/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit

class EpisodesViewController: UIViewController
{
    
    @IBOutlet weak var episodesImageView: UIImageView!
    @IBOutlet weak var descriptionsTextView: UITextView!
    @IBOutlet weak var titleTextView: UITextView!
    
    var object = [String: AnyObject]?()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //print(object)
        self.titleTextView.text = String(self.object![Skylark.episodes.title]!)
        self.episodesImageView.image = self.object![Skylark.episodes.downloadedImage] as? UIImage
        
        let newDescriptions = "\(String(self.object![Skylark.episodes.uid]!))\n\(String(self.object![Skylark.episodes.versionURL]!))\n\(String(self.object![Skylark.episodes.created]!))"
        
        self.descriptionsTextView.text = newDescriptions
        self.descriptionsTextView.textColor = UIColor.whiteColor()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.titleTextView.sizeToFit()
    }
    
}
