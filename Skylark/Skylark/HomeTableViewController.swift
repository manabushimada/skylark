//
//  HomeTableViewControllerr.swift
//  Skylark
//
//  Created by manabu shimada on 13/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

class HomeTableViewController: UITableViewController
{
    var objects = [JSON]()
    var images = [JSON]()

    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableView.alwaysBounceVertical = true
        
        self.setups()
        self.indicator.startAnimating()
        self.querySkylarkSetsAPI()
    }


    // MARK: API
    func querySkylarkSetsAPI()
    {
        NetworkManager.sharedInstance.requestWithURL(Skylark.setsAPI, completion:{ response in

            if response.result.isFailure
            {
                self.showNetworkErrorAlertView(response)
            }
            
            guard let object = response.result.value else {
                return
            }

            let json = JSON(object)
            json[Skylark.sets.objects].forEach { (_, json) in
                self.objects.append(json)
            }
            
            self.tableView.reloadData()
            self.indicator.stopAnimating()
            
        })
    }

    func getSetsImageURL(imageAPI: String,  completion: (imageURL: String) -> Void)
    {
        NetworkManager.sharedInstance.requestWithURL(Skylark.api+imageAPI, completion:{ response in
            
            if response.result.isFailure
            {
                self.showNetworkErrorAlertView(response)
            }
            
            guard let object = response.result.value else {
                return
            }
            
            let json = JSON(object)
            completion(imageURL: String(json[Skylark.sets.images.url]))
        })
    }

    
    

    // MARK: UITableView Delegates
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objects.count 
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "Cell"
        let cell: HomeTableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! HomeTableViewCell
        
        
        cell.updateTitleLabel(String(self.objects[indexPath.row][Skylark.sets.title]))
        cell.updateCreatedByLabel(String(self.objects[indexPath.row][Skylark.sets.createdBy]))
        cell.updateCreatedAtLabel(String(self.objects[indexPath.row][Skylark.sets.created]))
   
        
        
        
        // Set image
        let imageAPI = String(self.objects[indexPath.row][Skylark.sets.imageURLs][1])
        if imageAPI == "null"
        {
            // random images if no image
            cell.setsImageView.image = UIImage(named:Dummy.images[indexPath.row%Dummy.images.count])
        }
        else
        {
            self.getSetsImageURL(imageAPI, completion: { imageURL in
                cell.setsImageView.sd_setImageWithURL(NSURL(string: imageURL)!)
            })
        }
        
        return cell
        
    }
    
    
    // MARK: Overrides
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        let indexPath = self.tableView.indexPathForSelectedRow!
        let cell:HomeTableViewCell = self.tableView.cellForRowAtIndexPath(indexPath) as! HomeTableViewCell
        if (segue.identifier == "toHomeViewController")
        {
            /*----------------------------------------------------------------------------*
             * Pass the selected cell data.
             *----------------------------------------------------------------------------*/
            let vc = segue.destinationViewController as! HomeViewController
            vc.object = self.objects[indexPath.row].dictionaryObject
            vc.object![Skylark.sets.downloadedImage] = cell.setsImageView.image
        }
    }

    
    // MARK: Private Methods
    func showNetworkErrorAlertView(response: Response<AnyObject, NSError>)
    {
        self.indicator.stopAnimating()
        
        switch response.result
        {
        case .Failure(let encodingError):
             //print(encodingError)
            switch encodingError.code
            {
            case -1001, -1009: // 1009: offline, 1001: timeout
                let alertController = UIAlertController(title: Messages.alert.connectionErrorTitle, message:
                    Messages.alert.connectionErrorMessage, preferredStyle: UIAlertControllerStyle.Alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
          
                }))
                presentViewController(alertController, animated: true, completion: nil)
                break
            case 3840: // invalid values
                /**
                let alertController = UIAlertController(title: Messages.alert.invalidValues, message:
                    nil, preferredStyle: UIAlertControllerStyle.Alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                }))
                presentViewController(alertController, animated: true, completion: nil)
                */
                break
            default:
                break
               
            }
        default:
            break
        }
    }
    
    func setups()
    {
        /*----------------------------------------------------------------------------*
         * Add an indicator
         *----------------------------------------------------------------------------*/
        self.indicator = UIActivityIndicatorView()
        self.indicator.frame = CGRectMake(0, 0, 50, 50)
        self.indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.indicator.center = self.view.center
        self.view.addSubview(self.indicator)
        
        /*----------------------------------------------------------------------------*
         * Add a refreshcontrol
         *----------------------------------------------------------------------------*/
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(HomeTableViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl!)
    }
    

    func refresh()
    {
        self.querySkylarkSetsAPI()
        self.refreshControl!.endRefreshing()
    }
}

