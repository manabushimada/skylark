//
//  HomeViewController.swift
//  Skylark
//
//  Created by manabu shimada on 15/04/2016.
//  Copyright © 2016 manabu shimada. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeViewController: UIViewController
{
    
    @IBOutlet weak var setImageView: UIImageView!
    @IBOutlet weak var summaryTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var createdAtLabel: UILabel! 
    @IBOutlet weak var createdByLabel: UILabel!
    
    var object = [String: AnyObject]?()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        
        self.titleLabel.text = String(self.object![Skylark.sets.title]!)
        self.bodyTextView.text = String(self.object![Skylark.sets.body]!)
        self.bodyTextView.textColor = UIColor.whiteColor()
        
        self.setImageView.image = self.object![Skylark.sets.downloadedImage] as? UIImage
        
        let newCreatedAt = String(self.object![Skylark.sets.created]!)
        self.createdAtLabel.text = newCreatedAt.substringByIndex(10)

        let summary = String(self.object![Skylark.sets.summary]!)
        self.summaryTextView.text = (summary == "") ? "No summary" : summary
        self.summaryTextView.textColor = UIColor.whiteColor()
        
        let createdBy = String(self.object![Skylark.sets.createdBy]!)
        self.createdByLabel.text = (createdBy == "<null>") ? "Unknown" : createdBy
        
    }
    
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
         self.titleLabel.sizeToFit()
    }
}
